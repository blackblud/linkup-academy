# LinkUp Academy

## Task #1

Структура проекту (деякі файли доступні лише на окремій гілці task-scss):

- CSS - файли стилів.
- SCSS - файли стилів до рендеру в CSS.
- GRID - завдання з сіткою (додатково).
- index.html - головна сторінка сайту.

## Task #2

Основне завдання полягає у верстці лендінгу з добавленням респонсіву та анімацій.

1. Верстка лендінгу - повністю готова!
2. З респонсіву:

- Респонсів на width-1440px (Laptop L)
- Респонсів на width-1024px (Laptop)
- Респонсів на width-768px (Tablet)
- Респонсів на width-375px (Mobile)
-

3. З анімацій:

- Анімація заголовку "SALON"
- Текст "Coming Soon"
- Маркування нумерованих блоків (в тому числі і мобільна версія)
- Кругляшки на задньому фоні телефону
- Label до кожного з input'ів в формі

## Task #3

- Добавлено Favicon для різних типів девайсів на екранів
- Створено окремий шрифт для іконок

## Task #4

Загальний список функцій та їх призначення:

1. How Much is True

```
countTrue([true, false, false, true, false]) ➞ 2
countTrue([false, false, false, false]) ➞ 0
countTrue([]) ➞ 0
```

2. Instant JAZZ

```
jazzify(["G", "F", "C"]) ➞ ["G7", "F7", "C7"]
jazzify(["Dm", "G", "E", "A"]) ➞ ["Dm7", "G7", "E7", "A7"]
jazzify(["F7", "E7", "A7", "Ab7", "Gm7", "C7"]) ➞ ["F7", "E7", "A7", "Ab7", "Gm7", "C7"]
jazzify([]) ➞ []
```

3. Sort Numbers in Descending Order

```
sortDescending(123) ➞ 321
sortDescending(1254859723) ➞ 9875543221
sortDescending(73065) ➞ 76530
```

4. Sort an Array by String Length

```
sortByLength(["Google", "Apple", "Microsoft"])
➞ ["Apple", "Google", "Microsoft"]
sortByLength(["Leonardo", "Michelangelo", "Raphael", "Donatello"])
➞ ["Raphael", "Leonardo", "Donatello", "Michelangelo"]
sortByLength(["Turing", "Einstein", "Jung"])
➞ ["Jung", "Turing", "Einstein"]
```

5. Find the Smallest and Biggest Numbers

```
minMax([1, 2, 3, 4, 5]) ➞ [1, 5]
minMax([2334454, 5]) ➞ [5, 2334454]
minMax([1]) ➞ [1, 1]
```

6. Find the Largest Numbers in a Group of Arrays

```
findLargestNums([[4, 2, 7, 1], [20, 70, 40, 90], [1, 2, 0]]) ➞ [7, 90, 2]
findLargestNums([[-34, -54, -74], [-32, -2, -65], [-54, 7, -43]]) ➞ [-34, -2, 7]
findLargestNums([[0.4321, 0.7634, 0.652], [1.324, 9.32, 2.5423, 6.4314], [9, 3, 6, 3]]) ➞ [0.7634, 9.32, 9]
```

7. Simple OOP Calculator (NOT a function)

```
var calculator = new Calculator()
calculator.add(10, 5) ➞ 15
calculator.subtract(10, 5) ➞ 5
calculator.multiply(10, 5) ➞ 50
calculator.divide(10, 5) ➞ 2
```

8. Return the Objects Keys and Values

```
keysAndValues({ a: 1, b: 2, c: 3 }) ➞ [["a", "b", "c"], [1, 2, 3]]
keysAndValues({ a: "Apple", b: "Microsoft", c: "Google" })
➞ [["a", "b", "c"], ["Apple", "Microsoft", "Google"]]
keysAndValues({ key1: true, key2: false, key3: undefined })
➞ [["key1", "key2", "key3"], [true, false, undefined]]
```

9. Let's Sort This Array!

```
ascDesNone([4, 3, 2, 1], "Asc" ) ➞ [1, 2, 3, 4]
ascDesNone([7, 8, 11, 66], "Des") ➞ [66, 11, 8, 7]
ascDesNone([1, 2, 3, 4], "None") ➞ [1, 2, 3, 4]
```

10. Sort the Unsortable

```
sortIt([4, 1, 3]) ➞ [1, 3, 4]
sortIt([[4], [1], [3]]) ➞ [[1], [3], [4]]
sortIt([4, [1], 3]) ➞ [[1], 3, 4]
sortIt([[4], 1, [3]]) ➞ [1, [3], [4]]
sortIt([[3], 4, [2], [5], 1, 6]) ➞ [1, [2], [3], 4, [5], 6]
```

11. No Hidden Fees

```
hasHiddenFee(["$2", "$4", "$1", "$8"], "$15") ➞ false
hasHiddenFee(["$1", "$2", "$3"], "$6") ➞ false
hasHiddenFee(["$1"], "$4") ➞ true
```

12. Trace That Matrix

```
trace([
  [1, 0, 1, 0],
  [0, 2, 0, 2],
  [3, 0, 3, 0],
  [0, 4, 0, 4]
]) ➞ 10  // 1 + 2 + 3 + 4 = 10
```

13. Remove the Special Characters from a String (use RegExp)

```
removeSpecialCharacters("The quick brown fox!") ➞ "The quick brown fox"
removeSpecialCharacters("%fd76$fd(-)6GvKlO.") ➞ "fd76fd-6GvKlO"
removeSpecialCharacters("D0n$c sed 0di0 du1") ➞ "D0nc sed 0di0 du1"
```

14. Check if a String is a Mathematical Expression (use RegExp)

```
mathExpr("4 + 5") ➞ true
mathExpr("4*6") ➞ true
mathExpr("4*no") ➞ false
```

15. Pentagonal Number

```
pentagonal(1) ➞ 1
pentagonal(2) ➞ 6
pentagonal(3) ➞ 16
pentagonal(8) ➞ 141
```

16. Temperature Conversion

```
tempConversion(0) ➞ [32, 273.15] // 0°C is equal to 32°F and 273.15 K.
tempConversion(100) ➞ [212, 373.15]
tempConversion(-10) ➞ [14, 263.15]
tempConversion(300.4) ➞ [572.72, 573.55]
```

17. What's the Missing Letter?

```
missingLetter("abdefg") ➞ "c"
missingLetter("mnopqs") ➞ "r"
missingLetter("tuvxyz") ➞ "w"
missingLetter("ghijklmno") ➞ "No Missing Letter"
```

## Task #5 (in progress...)

Оновлений проект [Linkstagram](https://gitlab.com/blackblud/practice-react-project).
