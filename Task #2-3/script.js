// var block = document.getElementById('box1');
// var number = document.getElementById('num1');

// var myFunction = function () {
//   number.classList.add('num-anim');
// };

// block.addEventListener('mouseenter', myFunction, false);

var blocks = document.getElementsByClassName('text-block');
var numbers = document.getElementsByClassName('text-block-num');

for (let i = 0; i < blocks.length; i++) {
  blocks[i].addEventListener(
    'mouseenter',
    function () {
      numbers[i].classList.add('num-anim');
    },
    false,
  );
}
