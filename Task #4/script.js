// 1. How Much is True
function funct_1(array) {
  let count = 0;
  for (i = 0; i < array.length; i++) {
    if (array[i] == true) {
      count++;
    }
  }
  return count;
}

// 2. Instant JAZZ
function funct_2(array) {
  let new_array = [];
  for (i = 0; i < array.length; i++) {
    if (array[i].charAt(array[i].length - 1) != '7') {
      new_array[i] = array[i] + '7';
    } else {
      new_array[i] = array[i];
    }
  }

  return new_array;
}

// 3. Sort Numbers in Descending Order
function funct_3(number) {
  if (Math.sign(number) == -1) {
    return 'Error - Number is nonnegative';
  } else {
    let array = Array.from(String(number), Number);
    array.sort().reverse();

    let new_number = '';
    for (i = 0; i < array.length; i++) {
      new_number += array[i];
    }

    return new_number;
  }
}

// 4. Sort an Array by String Length
function funct_4(array) {
  return array.sort((x, y) => x.length - y.length);
}

// 5. Find the Smallest and Biggest Numbers
function funct_5(array) {
  return [Math.min.apply(null, array), Math.max.apply(null, array)];
}

// 6. Find the Largest Numbers in a Group of Arrays
function funct_6(array) {
  let new_array = [];

  for (i = 0; i < array.length; i++) {
    new_array.push(Math.max.apply(null, array[i]));
  }

  return new_array;
}

// 7. Simple OOP Calculator
class Calculator {
  add(a, b) {
    return a + b;
  }

  subtract(a, b) {
    return a - b;
  }

  multiply(a, b) {
    return a * b;
  }

  divide(a, b) {
    return a / b;
  }
}

// 8. Return the Objects Keys and Values
function funct_8(object) {
  let keys = [];
  let values = [];

  for (let [key, value] of Object.entries(object)) {
    keys.push(key);
    values.push(value);
  }

  return [keys, values];
}

// 9. Let's Sort This Array!
function funct_9(array, mode) {
  switch (mode) {
    case 'Asc':
      return array.sort((x, y) => x - y);
    case 'Des':
      return array.sort((x, y) => x - y).reverse();
    case 'None':
      return array;
    default:
      console.log('Error - Wrong mode');
  }
}

// 10. Sort the Unsortable
function funct_10(array) {
  return array.sort(); //тут якийсь підвох?
}

// 11. No Hidden Fees
function funct_11(prices, total) {
  let sum = 0;

  total = parseInt(total.substring(1));

  for (i = 0; i < prices.length; i++) {
    prices[i] = prices[i].substring(1);
    sum += parseInt(prices[i]);
  }

  if (sum == total) {
    return true;
  } else {
    return false;
  }
}

// 12. Trace That Matrix
function funct_12(matrix) {
  let sum = 0;

  for (i = 0; i < matrix.length; i++) {
    if (matrix.length != matrix[i].length) {
      return `Non Square Matrix! ${matrix.length} =! ${matrix[i].length}`;
    } else {
      sum += matrix[i][i];
    }
  }

  console.table(matrix);
  return sum;
}

// 13. Remove the Special Characters from a String (use RegExp)
function funct_13(string) {
  return string.replace(/[`~!@#$%^&*()|+\=?;:'",.<>\{\}\[\]\\\/]/gi, '');
}

// 14. Check if a String is a Mathematical Expression (use RegExp)
function funct_14(string) {
  if (string.match(/\d+(\+|\-|\*|\/)\d+/)) {
    return true;
  } else {
    return false;
  }
}

// 15. Pentagonal Number
function funct_15(number) {
  return (5 * number ** 2 - 5 * number + 2) / 2;
}

// 16. Temperature Conversion
function funct_16(celsius) {
  return [(celsius * 9) / 5 + 32, celsius + 273.15];
}

// 17. What's the Missing Letter?
function funct_17(string) {
  let alphabet = 'abcdefghijklmnopqrstuvwxyz';

  if (alphabet.includes(string)) {
    return 'No Missing Letter';
  } else {
    let index = alphabet.indexOf(string.charAt(0));

    for (i = 0; i < string.length; i++) {
      // console.log(
      //   'Compare [' +
      //     alphabet.indexOf(string.charAt(i)) +
      //     '] and [' +
      //     index +
      //     ']',
      // );
      if (alphabet.indexOf(string.charAt(i)) == index) {
        index++;
      } else {
        return alphabet.charAt(index);
      }
    }
  }
}

console.log('\n1. How Much is True');
console.log(
  '%c Input - [true, false, false, true, false]',
  'color: green; font-weight: bold;',
);
console.log(
  '%c Output - ' + funct_1([true, false, false, true, false]),
  'color: red; font-weight: bold;',
);

console.log('\n2. Instant JAZZ');
console.log(
  "%c Input - ['Dm', 'G', 'E7', 'A']",
  'color: green; font-weight: bold;',
);
console.log(
  '%c Output - ' + funct_2(['Dm', 'G', 'E7', 'A']),
  'color: red; font-weight: bold;',
);

console.log('\n3. Sort Numbers in Descending Order');
console.log('%c Input - 1254859723', 'color: green; font-weight: bold;');
console.log(
  '%c Output - ' + funct_3(1254859723),
  'color: red; font-weight: bold;',
);

console.log('\n4. Sort an Array by String Length');
console.log(
  "%c Input - 'Leonardo', 'Michelangelo', 'Raphael', 'Donatello'",
  'color: green; font-weight: bold;',
);
console.log(
  '%c Output - ' +
    funct_4(['Leonardo', 'Michelangelo', 'Raphael', 'Donatello']),
  'color: red; font-weight: bold;',
);

console.log('\n5. Find the Smallest and Biggest Numbers');
console.log('%c Input - [1, 2, 3, 4, 5]', 'color: green; font-weight: bold;');
console.log(
  '%c Output - ' + funct_5([1, 2, 3, 4, 5]),
  'color: red; font-weight: bold;',
);

console.log('\n6. Find the Largest Numbers in a Group of Arrays');
console.log(
  '%c Input - [4, 2, 7, 1], [20, 70, 40, 90], [1, 2, 0]',
  'color: green; font-weight: bold;',
);
console.log(
  '%c Output - ' +
    funct_6([
      [4, 2, 7, 1],
      [20, 70, 40, 90],
      [1, 2, 0],
    ]),
  'color: red; font-weight: bold;',
);

console.log('\n7. Simple OOP Calculator');
const Calc = new Calculator();

console.log('%c Input - 25, 5', 'color: green; font-weight: bold;');
console.log(
  '%c Output [Add] - ' + Calc.add(25, 5),
  'color: red; font-weight: bold;',
);
console.log(
  '%c Output [Subtract] - ' + Calc.subtract(25, 5),
  'color: red; font-weight: bold;',
);
console.log(
  '%c Output [Multiply] - ' + Calc.multiply(25, 5),
  'color: red; font-weight: bold;',
);
console.log(
  '%c Output [Divide] - ' + Calc.divide(25, 5),
  'color: red; font-weight: bold;',
);

console.log('\n8. Return the Objects Keys and Values');
console.log(
  '%c Input - { a: 1, b: 2, c: 3 }',
  'color: green; font-weight: bold;',
);
console.log(
  '%c Output - ' + funct_8({ a: 1, b: 2, c: 3 }),
  'color: red; font-weight: bold;',
);

console.log("\n9. Let's Sort This Array!");
console.log('%c Input - [4, 5, 2, 1]', 'color: green; font-weight: bold;');
console.log(
  '%c Output [Asc] - ' + funct_9([4, 5, 2, 1], 'Asc'),
  'color: red; font-weight: bold;',
);
console.log(
  '%c Output [Des] - ' + funct_9([4, 5, 2, 1], 'Des'),
  'color: red; font-weight: bold;',
);
console.log(
  '%c Output [None] - ' + funct_9([4, 5, 2, 1], 'None'),
  'color: red; font-weight: bold;',
);

console.log('\n10. Sort the Unsortable');
console.log(
  '%c Input - [4], [1], 2, [3], 0, 5',
  'color: green; font-weight: bold;',
);
console.log(
  '%c Output - ' + funct_10([[4], [1], 2, [3], 0, 5]),
  'color: red; font-weight: bold;',
);

console.log('\n11. No Hidden Fees');
console.log('%c Input - ($1, $2, $3 - $6)', 'color: green; font-weight: bold;');
console.log(
  '%c Output - ' + funct_11(['$1', '$2', '$3'], '$6'),
  'color: red; font-weight: bold;',
);
console.log('%c Input - ($1, $2, $3 - $7)', 'color: green; font-weight: bold;');
console.log(
  '%c Output - ' + funct_11(['$1', '$2', '$3'], '$7'),
  'color: red; font-weight: bold;',
);

console.log('\n12. Trace That Matrix');
console.log(
  '%c Input - [1, 0, 1, 0]\n\t\t[0, 4, 0, 2]\n\t\t[3, 0, 3, 0]\n\t\t[0, 4, 0, 4]',
  'color: green; font-weight: bold;',
);
console.log(
  '%c Output - ' +
    funct_12([
      [1, 0, 1, 0],
      [0, 4, 0, 2],
      [3, 0, 3, 0],
      [0, 4, 0, 4],
    ]),
  'color: red; font-weight: bold;',
);

console.log('\n13. Remove the Special Characters from a String (use RegExp)');
console.log(
  "%c Input - '%fd76$fd(-)6Gv_Kl*O.'",
  'color: green; font-weight: bold;',
);
console.log(
  '%c Output - ' + funct_13('%fd76$fd(-)6Gv_Kl*O.'),
  'color: red; font-weight: bold;',
);

console.log(
  '\n14. Check if a String is a Mathematical Expression (use RegExp)',
);
console.log("%c Input - '4+5'", 'color: green; font-weight: bold;');
console.log('%c Output - ' + funct_14('4+5'), 'color: red; font-weight: bold;');
console.log("%c Input - '4*no'", 'color: green; font-weight: bold;');
console.log(
  '%c Output - ' + funct_14('4*no'),
  'color: red; font-weight: bold;',
);

console.log('\n15. Pentagonal Number');
console.log('%c Input - 8', 'color: green; font-weight: bold;');
console.log('%c Output - ' + funct_15(8), 'color: red; font-weight: bold;');

console.log('\n16. Temperature Conversion');
console.log('%c Input - 300.4', 'color: green; font-weight: bold;');
console.log('%c Output - ' + funct_16(300.4), 'color: red; font-weight: bold;');

console.log("\n17. What's the Missing Letter?");
console.log("%c Input - 'abcdef'", 'color: green; font-weight: bold;');
console.log(
  '%c Output - ' + funct_17('abcdef'),
  'color: red; font-weight: bold;',
);
console.log("%c Input - 'mnopqs'", 'color: green; font-weight: bold;');
console.log(
  '%c Output - ' + funct_17('mnopqs'),
  'color: red; font-weight: bold;',
);
