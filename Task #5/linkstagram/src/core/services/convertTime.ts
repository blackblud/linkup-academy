const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

export default function convertTime(data: string): string {
  let minutes: string | number;

  if (new Date(data).getUTCMinutes() < 10) {
    minutes = `${new Date(data).getUTCMinutes()}0`;
  } else {
    minutes = new Date(data).getUTCMinutes();
  }

  return `${new Date(data).getDate()} ${
    monthNames[new Date(data).getMonth()]
  } - ${new Date(data).getUTCHours()}:${minutes}`;
}
