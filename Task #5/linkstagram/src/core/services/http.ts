interface DataRequest {
  headers?: {
    Authorization?: string;
  };
  body?: string;
}

export default function http(
  method: string,
  endpoint: string,
  dataRequest: DataRequest = {},
): Promise<Response> {
  let dataSource;

  if (localStorage.getItem('Authorization')) {
    dataSource = {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: String(localStorage.getItem('Authorization')),
      },
    };
  } else {
    dataSource = {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    };
  }

  const data = {
    ...dataSource,
    ...dataRequest,
    headers: { ...dataSource.headers, ...dataRequest.headers },
  };

  return fetch(endpoint, data);
}
