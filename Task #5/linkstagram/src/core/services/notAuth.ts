export default function notAuth(): void {
  alert('You are not logged in.');
}
