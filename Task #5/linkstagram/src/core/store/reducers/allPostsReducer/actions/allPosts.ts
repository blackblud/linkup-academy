import { Dispatch } from 'react';

import http from '../../../../services/http';
import { AllPostAction, SET_POSTS } from '../../../types/allPosts';

const fetchAllPosts = () => {
  return async (dispatch: Dispatch<AllPostAction>): Promise<void> => {
    const res = await http('GET', 'https://linkstagram-api.linkupst.com/posts');

    const allPosts = await res.json();

    dispatch({
      type: SET_POSTS,
      payload: {
        posts: allPosts,
        loading: false,
      },
    });
  };
};

export default fetchAllPosts;
