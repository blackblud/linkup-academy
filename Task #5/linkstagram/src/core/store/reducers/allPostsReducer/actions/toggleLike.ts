import { Dispatch } from 'react';

import {
  AllPostAction,
  AllPostState,
  SET_POSTS,
} from '../../../types/allPosts';

const toggleLikee = (id: number, posts1: AllPostState, method: string) => {
  return (dispatch: Dispatch<AllPostAction>): void => {
    const newAllPosts = posts1.posts.map((post) => {
      if (post.id === id) {
        if (method === 'SET') {
          return {
            ...post,
            is_liked: !post.is_liked,
            likes_count: post.likes_count + 1,
          };
        }
        return {
          ...post,
          is_liked: !post.is_liked,
          likes_count: post.likes_count - 1,
        };
      }
      return post;
    });

    dispatch({
      type: SET_POSTS,
      payload: {
        posts: newAllPosts,
        loading: false,
      },
    });
  };
};

export default toggleLikee;
