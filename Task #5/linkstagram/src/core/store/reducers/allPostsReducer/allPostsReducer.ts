import { AllPostAction, AllPostState, SET_POSTS } from '../../types/allPosts';

const initialState: AllPostState = {
  posts: [],
  loading: true,
};

const allPostReducer = (
  state = initialState,
  action: AllPostAction,
): AllPostState => {
  switch (action.type) {
    case SET_POSTS:
      return {
        posts: action?.payload?.posts,
        loading: action?.payload?.loading,
      };

    default:
      return state;
  }
};

export default allPostReducer;
