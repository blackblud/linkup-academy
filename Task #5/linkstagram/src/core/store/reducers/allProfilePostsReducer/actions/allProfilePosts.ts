import { Dispatch } from 'react';

import http from '../../../../services/http';
import {
  AllProfilePostAction,
  SET_PROFILE_POSTS,
} from '../../../types/allProfilePosts';

const fetchAllProfilePosts = (username: string) => {
  return async (dispatch: Dispatch<AllProfilePostAction>): Promise<void> => {
    const res = await http(
      'GET',
      `https://linkstagram-api.linkupst.com/profiles/${username}/posts`,
    );

    const allProfilePosts = await res.json();

    dispatch({
      type: SET_PROFILE_POSTS,
      payload: {
        posts: allProfilePosts,
        loading: false,
      },
    });
  };
};

export default fetchAllProfilePosts;
