import {
  AllProfilePostAction,
  AllProfilePostState,
  SET_PROFILE_POSTS,
} from '../../types/allProfilePosts';

const initialState: AllProfilePostState = {
  posts: [],
  loading: true,
};

const allProfilePostReducer = (
  state = initialState,
  action: AllProfilePostAction,
): AllProfilePostState => {
  switch (action.type) {
    case SET_PROFILE_POSTS:
      return {
        posts: action?.payload?.posts,
        loading: action?.payload?.loading,
      };

    default:
      return state;
  }
};

export default allProfilePostReducer;
