import { Dispatch } from 'react';

import http from '../../../../services/http';
import { AllUsersAction, SET_USERS } from '../../../types/allUsers';

const fetchAllUsers = () => {
  return async (dispatch: Dispatch<AllUsersAction>): Promise<void> => {
    const res = await http(
      'GET',
      'https://linkstagram-api.linkupst.com/profiles',
    );
    const allUsers = await res.json();

    dispatch({
      type: SET_USERS,
      payload: {
        users: allUsers,
        loading: false,
      },
    });
  };
};

export default fetchAllUsers;
