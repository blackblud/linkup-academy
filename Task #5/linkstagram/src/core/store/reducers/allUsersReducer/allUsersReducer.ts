import { AllUsersAction, AllUsersState, SET_USERS } from '../../types/allUsers';

const initialState: AllUsersState = {
  users: [],
  loading: true,
};

const allUsersReducer = (
  state = initialState,
  action: AllUsersAction,
): AllUsersState => {
  switch (action.type) {
    case SET_USERS:
      return {
        users: action?.payload?.users,
        loading: action?.payload?.loading,
      };

    default:
      return state;
  }
};

export default allUsersReducer;
