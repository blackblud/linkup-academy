import { Dispatch } from 'react';

import { CLEAR_COMMENTS, CommentsAction } from '../../../types/comments';

const clearCommentsInfo = () => {
  return async (dispatch: Dispatch<CommentsAction>): Promise<void> => {
    dispatch({
      type: CLEAR_COMMENTS,
      payload: {
        comments: [],
      },
    });
  };
};

export default clearCommentsInfo;
