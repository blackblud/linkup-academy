import { Dispatch } from 'react';

import http from '../../../../services/http';
import { CommentsAction, GET_COMMENTS } from '../../../types/comments';

const fetchCommentsInfo = (id: string) => {
  return async (dispatch: Dispatch<CommentsAction>): Promise<void> => {
    const res = await http(
      'GET',
      `https://linkstagram-api.linkupst.com/posts/${id}/comments`,
    );

    const commentsInfo = await res.json();

    dispatch({
      type: GET_COMMENTS,
      payload: {
        comments: commentsInfo,
      },
    });
  };
};

export default fetchCommentsInfo;
