import {
  GET_COMMENTS,
  CommentsAction,
  CommentsState,
  CLEAR_COMMENTS,
} from '../../types/comments';

const initialState: CommentsState = {
  comments: [],
};

const commentsReducer = (
  state = initialState,
  action: CommentsAction,
): CommentsState => {
  switch (action.type) {
    case GET_COMMENTS:
      return {
        ...state,
        comments: action?.payload?.comments || state.comments,
      };

    case CLEAR_COMMENTS:
      return {
        ...state,
        comments: action?.payload?.comments,
      };

    default:
      return state;
  }
};

export default commentsReducer;
