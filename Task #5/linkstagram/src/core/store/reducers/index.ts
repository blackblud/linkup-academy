import { combineReducers } from 'redux';

import allPostsReducer from './allPostsReducer/allPostsReducer';
import allProfilePostsReducer from './allProfilePostsReducer/allProfilePostsReducer';
import allUsersReducer from './allUsersReducer/allUsersReducer';
import commentsReducer from './commentsReducer/commentsReducer';
import postReducer from './postReducer/postReducer';
import userReducer from './userReducer/userReducer';

const rootReducer = combineReducers({
  user: userReducer,
  users: allUsersReducer,
  post: postReducer,
  posts: allPostsReducer,
  comments: commentsReducer,
  profilePosts: allProfilePostsReducer,
});

export default rootReducer;
