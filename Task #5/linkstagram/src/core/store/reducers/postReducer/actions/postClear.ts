import { Dispatch } from 'react';

import CatLoading from '../../../../../public/images/loading/cat-anim.svg';
import { GET_POST, PostAction } from '../../../types/post';

const clearPostInfo = () => {
  return async (dispatch: Dispatch<PostAction>): Promise<void> => {
    dispatch({
      type: GET_POST,
      payload: {
        id: 0,
        author: {
          username: '',
          description: '',
          first_name: '',
          followers: 0,
          following: 0,
          job_title: '',
          last_name: '',
          profile_photo_url: '',
        },
        comments_count: 0,
        created_at: '',
        description: '',
        is_liked: false,
        likes_count: 0,
        photos: [{ id: 0, url: CatLoading }],
      },
    });
  };
};

export default clearPostInfo;
