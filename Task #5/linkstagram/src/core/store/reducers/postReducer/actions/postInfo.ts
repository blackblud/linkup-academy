import { Dispatch } from 'react';

import http from '../../../../services/http';
import { GET_POST, PostAction } from '../../../types/post';

const fetchPostInfo = (id: string) => {
  return async (dispatch: Dispatch<PostAction>): Promise<void> => {
    const res = await http(
      'GET',
      `https://linkstagram-api.linkupst.com/posts/${id}`,
    );

    const postInfo = await res.json();

    dispatch({
      type: GET_POST,
      payload: {
        id: postInfo.id,
        author: {
          username: postInfo.author.username,
          description: postInfo.author.description,
          first_name: postInfo.author.first_name,
          followers: postInfo.author.followers,
          following: postInfo.author.following,
          job_title: postInfo.author.job_title,
          last_name: postInfo.author.last_name,
          profile_photo_url: postInfo.author.profile_photo_url,
        },
        comments_count: postInfo.comments_count,
        created_at: postInfo.created_at,
        description: postInfo.description,
        is_liked: postInfo.is_liked,
        likes_count: postInfo.likes_count,
        photos: postInfo.photos,
      },
    });
  };
};

export default fetchPostInfo;
