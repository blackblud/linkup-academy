import CatLoading from '../../../../public/images/loading/cat-anim.svg';
import { GET_POST, PostAction, PostState } from '../../types/post';

const initialState: PostState = {
  id: 0,
  author: {
    username: '',
    description: '',
    first_name: '',
    followers: 0,
    following: 0,
    job_title: '',
    last_name: '',
    profile_photo_url: '',
  },
  comments_count: 0,
  created_at: '',
  description: '',
  is_liked: false,
  likes_count: 0,
  photos: [{ id: 0, url: CatLoading }],
};

const postReducer = (state = initialState, action: PostAction): PostState => {
  switch (action.type) {
    case GET_POST:
      return {
        id: action.payload.id,
        author: {
          username: action.payload.author.username,
          description: action.payload.author.description,
          first_name: action.payload.author.first_name,
          followers: action.payload.author.followers,
          following: action.payload.author.following,
          job_title: action.payload.author.job_title,
          last_name: action.payload.author.last_name,
          profile_photo_url: action.payload.author.profile_photo_url,
        },
        comments_count: action.payload.comments_count,
        created_at: action.payload.created_at,
        description: action.payload.description,
        is_liked: action?.payload?.is_liked,
        likes_count: action.payload.likes_count,
        photos: action.payload.photos,
      };

    default:
      return state;
  }
};

export default postReducer;
