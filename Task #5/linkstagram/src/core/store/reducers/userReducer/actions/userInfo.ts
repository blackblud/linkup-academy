import { Dispatch } from 'react';

import http from '../../../../services/http';
import { UPDATE_USER, UserAction } from '../../../types/user';

const fetchUserInfo = () => {
  return async (dispatch: Dispatch<UserAction>): Promise<void> => {
    const res = await http(
      'GET',
      'https://linkstagram-api.linkupst.com/account',
    );

    const userInfo = await res.json();

    dispatch({
      type: UPDATE_USER,
      payload: {
        username: userInfo.username,
        first_name: userInfo.first_name,
        last_name: userInfo.last_name,
        followers: userInfo.followers,
        following: userInfo.following,
        job_title: userInfo.job_title,
        description: userInfo.description,
        profile_photo_url: userInfo.profile_photo_url,
      },
    });
  };
};

export default fetchUserInfo;
