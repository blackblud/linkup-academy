import profilePlaceholder from '../../../../public/images/placeholders/profilePlaceholder.png';
import { UPDATE_USER, UserAction, UserState } from '../../types/user';

const initialState: UserState = {
  username: '',
  first_name: '',
  last_name: '',
  followers: 0,
  following: 0,
  job_title: '',
  description: '',
  profile_photo_url: profilePlaceholder,
};

const userReducer = (state = initialState, action: UserAction): UserState => {
  switch (action.type) {
    case UPDATE_USER:
      return {
        username: action?.payload?.username || state.username,
        first_name: action?.payload?.first_name || state.first_name,
        last_name: action?.payload?.last_name || state.last_name,
        followers: action?.payload?.followers || state.followers,
        following: action?.payload?.following || state.following,
        job_title: action?.payload?.job_title || state.job_title,
        description: action?.payload?.description || state.description,
        profile_photo_url:
          action?.payload?.profile_photo_url || state.profile_photo_url,
      };

    default:
      return state;
  }
};

export default userReducer;
