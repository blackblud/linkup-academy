import { PostState } from './post';

export const SET_POSTS = 'SET_POSTS';

export interface AllPostState {
  posts: PostState[];
  loading: boolean;
}

export interface AllPostAction {
  type: string;
  payload: AllPostState;
}
