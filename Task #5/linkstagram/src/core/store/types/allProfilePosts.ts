import { PostState } from './post';

export const SET_PROFILE_POSTS = 'SET_PROFILE_POSTS';

export interface AllProfilePostState {
  posts: PostState[];
  loading: boolean;
}

export interface AllProfilePostAction {
  type: string;
  payload: AllProfilePostState;
}
