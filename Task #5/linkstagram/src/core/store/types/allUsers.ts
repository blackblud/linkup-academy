import { UserState } from './user';

export const SET_USERS = 'SET_USERS';

export interface AllUsersState {
  users: UserState[];
  loading: boolean;
}

export interface AllUsersAction {
  type: string;
  payload: AllUsersState;
}
