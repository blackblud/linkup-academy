import { UserState } from './user';

export const GET_COMMENTS = 'GET_COMMENTS';
export const CLEAR_COMMENTS = 'CLEAR_COMMENTS';

export interface OneCommentState {
  id: number;
  commenter: UserState;
  created_at: string;
  message: string;
}

export interface CommentsState {
  comments: OneCommentState[];
}

export interface CommentsAction {
  type: string;
  payload: CommentsState;
}
