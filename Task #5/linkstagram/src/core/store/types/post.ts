import { UserState } from './user';

export const GET_POST = 'GET_POST';

interface PostPhotos {
  id: number;
  url: string;
}

export interface PostState {
  id: number;
  author: UserState;
  comments_count: number;
  created_at: string;
  description: string | null;
  is_liked: boolean;
  likes_count: number;
  photos: PostPhotos[];
}

export interface PostAction {
  type: string;
  payload: PostState;
}
