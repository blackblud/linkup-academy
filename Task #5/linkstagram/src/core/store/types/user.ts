export const UPDATE_USER = 'UPDATE_USER';

export interface UserState {
  username: string;
  description: string | null;
  first_name: string | null;
  followers: number;
  following: number;
  job_title: string | null;
  last_name: string | null;
  profile_photo_url: string | null;
}

export interface UserAction {
  type: string;
  payload: UserState;
}
