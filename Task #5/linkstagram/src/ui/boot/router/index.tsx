import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import '../../style/main.scss';

import LoginHeader from '../../components/header/LoginHeader';
import MainHeader from '../../components/header/MainHeader';
import Home from '../../pages/Home/Home';
import NotFound404 from '../../pages/NotFound404/NotFound404';
import Profile from '../../pages/Profile/Profile';
import SignIn from '../../pages/Sign/SignIn';
import SignUp from '../../pages/Sign/SignUp';
import EditProfileView from '../../views/Popup/EditProfile';
import NewPost from '../../views/Popup/NewPost';
import ViewPost from '../../views/Popup/ViewPost';

export default function Index(): JSX.Element {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/signin" component={LoginHeader} />
          <Route exact path="/signup" component={LoginHeader} />
          <Route component={MainHeader} />
        </Switch>
        <Switch>
          <Route exact path="/">
            <Redirect to="/posts" />
          </Route>
          <Route exact path="/signin" component={SignIn} />
          <Route exact path="/signup" component={SignUp} />
          <Route exact path="/profile" component={Profile} />
          <Route exact path="/profile/*" component={Profile} />
          <Route exact path="/posts" component={Home} />
          <Route exact path="/posts/*" component={Home} />
          <Route exact path="*" component={NotFound404} />
        </Switch>
        <Switch>
          <Route
            exact
            path={['/profile/new', '/posts/new']}
            component={NewPost}
          />
          <Route
            exact
            path={['/profile/edit', '/posts/edit']}
            component={EditProfileView}
          />
          <Route exact path="/posts/:id" component={ViewPost} />
        </Switch>
      </Router>
    </div>
  );
}
