import React from 'react';

import '../../style/header.scss';

export default function SignHeader(): JSX.Element {
  return (
    <header>
      <p className="logo">Linkstagram</p>

      <button className="btn-lang" type="button">
        <p>EN</p>
      </button>
    </header>
  );
}
