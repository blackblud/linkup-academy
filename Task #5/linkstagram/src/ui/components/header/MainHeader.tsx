import React from 'react';
import { Link, useLocation } from 'react-router-dom';

import notAuth from '../../../core/services/notAuth';
import profilePlaceholder from '../../../public/images/placeholders/profilePlaceholder.png';
import useTypedSelector from '../../hooks/useTypedSelector';

export default function MainHeader(): JSX.Element {
  const userPhotoUrl = useTypedSelector(
    (state) => state.user.profile_photo_url,
  );

  const location = useLocation();

  return (
    <header>
      <p className="logo">Linkstagram</p>

      <div className="nav-header">
        <Link to="/" className="link-home">
          <p className="homeText">Home</p>
        </Link>

        <button className="btn-lang" type="button">
          <p>EN</p>
        </button>

        {location.pathname === '/profile' ? (
          <>
            <Link to="/posts" className="link-home-light">
              <p>Home</p>
            </Link>
            {localStorage.getItem('Authorization') ? (
              <Link to="/profile" className="link-user1">
                {userPhotoUrl !== null ? (
                  <img src={userPhotoUrl} alt="ProfileMini" />
                ) : (
                  <img src={profilePlaceholder} alt="ProfileMini" />
                )}
              </Link>
            ) : (
              <>
                <button type="button" onClick={notAuth} className="link-user">
                  <img src={profilePlaceholder} alt="ProfileMini" />
                </button>
              </>
            )}
          </>
        ) : (
          <>
            {localStorage.getItem('Authorization') ? (
              <Link to="/profile" className="link-user">
                {userPhotoUrl !== null ? (
                  <img src={userPhotoUrl} alt="ProfileMini" />
                ) : (
                  <img src={profilePlaceholder} alt="ProfileMini" />
                )}
              </Link>
            ) : (
              <>
                <button type="button" onClick={notAuth} className="link-user">
                  <img src={profilePlaceholder} alt="ProfileMini" />
                </button>
              </>
            )}
          </>
        )}
      </div>
    </header>
  );
}
