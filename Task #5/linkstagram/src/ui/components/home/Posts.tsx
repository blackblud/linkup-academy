import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import fetchAllPosts from '../../../core/store/reducers/allPostsReducer/actions/allPosts';
import useTypedSelector from '../../hooks/useTypedSelector';
import Post from '../parts/Post';

export default function Posts(): JSX.Element {
  const dispatch = useDispatch();
  const { posts, loading } = useTypedSelector((state) => state.posts);

  useEffect(() => {
    dispatch(fetchAllPosts());
  }, [dispatch]);

  if (loading) {
    return (
      <div className="posts">
        <div className="container">
          <div className="loading">
            Posts&nbsp;
            <div className="loading__letter">L</div>
            <div className="loading__letter">o</div>
            <div className="loading__letter">a</div>
            <div className="loading__letter">d</div>
            <div className="loading__letter">i</div>
            <div className="loading__letter">n</div>
            <div className="loading__letter">g</div>
            <div className="loading__letter">.</div>
            <div className="loading__letter">.</div>
            <div className="loading__letter">.</div>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="posts">
      {posts.map((post) => (
        <Post data={post} key={post.id} />
      ))}
    </div>
  );
}
