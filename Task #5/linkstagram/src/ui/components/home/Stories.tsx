import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import fetchAllUsers from '../../../core/store/reducers/allUsersReducer/actions/allUsers';
import useTypedSelector from '../../hooks/useTypedSelector';
import Story from '../parts/Story';

export default function Stories(): JSX.Element {
  const dispatch = useDispatch();
  const { users, loading } = useTypedSelector((state) => state.users);

  useEffect(() => {
    dispatch(fetchAllUsers());
  }, [dispatch]);

  if (loading) {
    return (
      <div className="stories">
        <div className="container">
          <div className="loading">
            Story&nbsp;
            <div className="loading__letter">L</div>
            <div className="loading__letter">o</div>
            <div className="loading__letter">a</div>
            <div className="loading__letter">d</div>
            <div className="loading__letter">i</div>
            <div className="loading__letter">n</div>
            <div className="loading__letter">g</div>
            <div className="loading__letter">.</div>
            <div className="loading__letter">.</div>
            <div className="loading__letter">.</div>
          </div>
        </div>
      </div>
    );
  }
  return (
    <div className="stories">
      {users.map((user) => (
        <Story data={user} key={user.username} />
      ))}
    </div>
  );
}
