import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import notAuth from '../../../core/services/notAuth';
import fetchUserInfo from '../../../core/store/reducers/userReducer/actions/userInfo';
import profilePlaceholder from '../../../public/images/placeholders/profilePlaceholder.png';
import useTypedSelector from '../../hooks/useTypedSelector';

export default function UserInfo(): JSX.Element {
  const dispatch = useDispatch();
  const user = useTypedSelector((state) => state.user);

  useEffect(() => {
    if (localStorage.getItem('Authorization')) {
      dispatch(fetchUserInfo());
    }
  }, [dispatch]);

  return (
    <div className="user-info">
      <div className="data-info">
        <div className="data-followers">
          <p>{user.followers}</p>
          <p>Followers</p>
        </div>
        <div className="data-profilePic">
          <div>
            {user.profile_photo_url !== null ? (
              <img src={user.profile_photo_url} alt="ProfileMini" />
            ) : (
              <img src={profilePlaceholder} alt="ProfileMini" />
            )}

            {localStorage.getItem('Authorization') ? (
              <Link to="posts/new">
                <FontAwesomeIcon icon={faPlus} className="icon-plus" />
              </Link>
            ) : (
              <button type="button" onClick={notAuth}>
                <FontAwesomeIcon icon={faPlus} className="icon-plus" />
              </button>
            )}
          </div>
        </div>
        <div className="data-following">
          <p>{user.following}</p>
          <p>Following</p>
        </div>
      </div>

      <div className="name-info">
        <p>{`${user.first_name} ${user.last_name}`}</p>
        <span>-</span>
        <p>{user.job_title}</p>
      </div>

      <div className="bio-info">
        <p>{user.description}</p>
      </div>

      <div className="button-info">
        <Link to="posts/edit" className="btn-edit-profile">
          Edit Profile
        </Link>
        {localStorage.getItem('Authorization') ? (
          <>
            <Link to="posts/new" className="btn-new-post">
              New Post
            </Link>
          </>
        ) : (
          <>
            <button type="button" onClick={notAuth} className="btn-new-post">
              New Post
            </button>
          </>
        )}
      </div>

      <footer>
        <div>
          <Link to="/about">About</Link>
          <Link to="/help">Help</Link>
          <Link to="/privacy">Privacy</Link>
          <Link to="/terms">Terms</Link>
          <Link to="/location">Location</Link>
          <Link to="/language">Language</Link>
        </div>
        <p>&copy; 2021 Linkstagram</p>
      </footer>
    </div>
  );
}
