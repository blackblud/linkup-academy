import React from 'react';

import convertTime from '../../../core/services/convertTime';
import { OneCommentState } from '../../../core/store/types/comments';
import ProfilePlaceholder from '../../../public/images/placeholders/profilePlaceholder.png';

interface CommentsProps {
  data: OneCommentState;
}

export default function OneComment({ data }: CommentsProps): JSX.Element {
  return (
    <div className="comment">
      {data.commenter.profile_photo_url !== null ? (
        <img src={data.commenter.profile_photo_url} alt="ProfileMini" />
      ) : (
        <img src={ProfilePlaceholder} alt="ProfileMini" />
      )}

      <div className="comment-data">
        <p className="data-message">{data.message}</p>
        <p className="data-time">{convertTime(data.created_at)}</p>
      </div>
    </div>
  );
}
