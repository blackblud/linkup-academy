import {
  faHeart,
  faComment,
  faArrowRight,
  faEllipsisV,
  faLink,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import convertTime from '../../../core/services/convertTime';
import http from '../../../core/services/http';
import notAuth from '../../../core/services/notAuth';
import toggleLikee from '../../../core/store/reducers/allPostsReducer/actions/toggleLike';
import { PostState } from '../../../core/store/types/post';
import postPlaceholder from '../../../public/images/placeholders/postPlaceholder.png';
import ProfilePlaceholder from '../../../public/images/placeholders/profilePlaceholder.png';
import useTypedSelector from '../../hooks/useTypedSelector';

interface PostProps {
  data: PostState;
}

export default function OnePost({ data }: PostProps): JSX.Element {
  const [copied, setCopied] = useState<string>();
  const dispatch = useDispatch();
  const posts1 = useTypedSelector((state) => state.posts);

  function getPostURL() {
    navigator.clipboard.writeText(`${window.location.href}/${data.id}`);
    setCopied('share-anim');
    setTimeout(() => {
      setCopied(' ');
    }, 1500);
  }

  function toggleLike(): void {
    if (!localStorage.getItem('Authorization')) {
      notAuth();
    }

    const dataRequest = {
      headers: {
        Authorization: String(localStorage.getItem('Authorization')),
      },
    };

    if (data.is_liked) {
      http(
        'DELETE',
        `https://linkstagram-api.linkupst.com/posts/${data.id}/like`,
        dataRequest,
      );
      dispatch(toggleLikee(data.id, posts1, 'REMOVE'));
    } else {
      http(
        'POST',
        `https://linkstagram-api.linkupst.com/posts/${data.id}/like`,
        dataRequest,
      );
      dispatch(toggleLikee(data.id, posts1, 'SET'));
    }
  }

  return (
    <div className="post">
      <div className="post-header">
        <div className="post-author">
          <img
            src={
              data.author.profile_photo_url !== null
                ? data.author.profile_photo_url
                : ProfilePlaceholder
            }
            alt="Post ProfileMini"
          />

          <div>
            <p>{`${data.author.first_name} ${data.author.last_name}`}</p>
            <p>{convertTime(data.created_at)}</p>
          </div>
        </div>
        <div className="post-dots">
          <FontAwesomeIcon icon={faEllipsisV} className="icon-dots" />
        </div>
      </div>

      <div className="post-body">
        <div className="post-image">
          <img
            src={
              data.photos.length === 0 ? postPlaceholder : data.photos[0].url
            }
            alt="Post"
          />

          <div className={`share ${copied}`}>
            <FontAwesomeIcon icon={faLink} />
            &nbsp;Copied to Buffer !
          </div>
        </div>
        <div className="post-desc">
          <p>{data.description}</p>
        </div>
      </div>

      <div className="post-footer">
        <div className="post-controls">
          {data.is_liked ? (
            <FontAwesomeIcon
              icon={faHeart}
              className="icon-liked"
              onClick={() => toggleLike()}
            />
          ) : (
            <FontAwesomeIcon
              icon={faHeart}
              className="icon-like"
              onClick={() => toggleLike()}
            />
          )}

          <p>{data.likes_count}</p>

          {localStorage.getItem('Authorization') ? (
            <Link to={`/posts/${data.id}`}>
              <FontAwesomeIcon icon={faComment} className="icon-comment" />
            </Link>
          ) : (
            <button type="button" onClick={notAuth} className="not-auth">
              <FontAwesomeIcon icon={faComment} className="icon-comment" />
            </button>
          )}

          <p>{data.comments_count}</p>
        </div>
        <button type="button" className="post-share" onClick={getPostURL}>
          Share
          <FontAwesomeIcon icon={faArrowRight} className="icon-share" />
        </button>
      </div>
    </div>
  );
}
