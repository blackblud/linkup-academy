import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { useDispatch } from 'react-redux';

import http from '../../../core/services/http';
import fetchAllProfilePosts from '../../../core/store/reducers/allProfilePostsReducer/actions/allProfilePosts';
import { PostState } from '../../../core/store/types/post';

interface ProfilePostProps {
  data: PostState;
}

export default function OneProfilePost({
  data,
}: ProfilePostProps): JSX.Element {
  const dispatch = useDispatch();

  async function deletePost() {
    const choice = window.confirm(`Delete Post with ID #${data.id} ?`);

    const dataRequest = {
      headers: {
        Authorization: String(localStorage.getItem('Authorization')),
      },
    };

    if (choice) {
      const res = await http(
        'DELETE',
        `https://linkstagram-api.linkupst.com/posts/${data.id}`,
        dataRequest,
      );

      if (res.ok) {
        dispatch(fetchAllProfilePosts(data.author.username));
      } else {
        alert('Error while Deleting Photo. Please try again.');
      }
    }
  }

  return (
    <div className="profile-post-one" key={data.id}>
      <img src={data.photos[0].url} alt="Post" />
      <button
        type="button"
        className="profile-post-delete"
        onClick={deletePost}
      >
        <p>Delete</p>
        <FontAwesomeIcon icon={faTrashAlt} className="icon-delete" />
      </button>
    </div>
  );
}
