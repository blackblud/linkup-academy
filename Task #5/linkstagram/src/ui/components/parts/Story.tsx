import { UserState } from '../../../core/store/types/user';
import StoryPlaceholder1 from '../../../public/images/placeholders/stories/storyPlaceholder_1.png';
import StoryPlaceholder2 from '../../../public/images/placeholders/stories/storyPlaceholder_2.png';
import StoryPlaceholder3 from '../../../public/images/placeholders/stories/storyPlaceholder_3.png';
import StoryPlaceholder4 from '../../../public/images/placeholders/stories/storyPlaceholder_4.png';
import StoryPlaceholder5 from '../../../public/images/placeholders/stories/storyPlaceholder_5.png';
import StoryPlaceholder6 from '../../../public/images/placeholders/stories/storyPlaceholder_6.png';
import StoryPlaceholder7 from '../../../public/images/placeholders/stories/storyPlaceholder_7.png';
import StoryPlaceholder8 from '../../../public/images/placeholders/stories/storyPlaceholder_8.png';
import StoryPlaceholder9 from '../../../public/images/placeholders/stories/storyPlaceholder_9.png';

interface AllUsersProps {
  data: UserState;
}

export default function OneStory({ data }: AllUsersProps): JSX.Element {
  const StoryPlaceholders = [
    StoryPlaceholder1,
    StoryPlaceholder2,
    StoryPlaceholder3,
    StoryPlaceholder4,
    StoryPlaceholder5,
    StoryPlaceholder6,
    StoryPlaceholder7,
    StoryPlaceholder8,
    StoryPlaceholder9,
  ];

  return (
    <div className="story">
      <div>
        {data.profile_photo_url != null ? (
          <img src={data.profile_photo_url} alt="Story" />
        ) : (
          <img
            src={
              StoryPlaceholders[
                Math.floor(Math.random() * StoryPlaceholders.length)
              ]
            }
            alt="Story"
          />
        )}
      </div>
    </div>
  );
}
