import React from 'react';

import Posts from '../../components/home/Posts';
import Stories from '../../components/home/Stories';
import UserInfo from '../../components/home/UserInfo';

import '../../style/home.scss';

export default function Home(): JSX.Element {
  return (
    <>
      <div className="wrapper-home">
        <div className="home-content">
          <Stories />
          <Posts />
        </div>
        <div className="home-user-info">
          <UserInfo />
        </div>
      </div>
    </>
  );
}
