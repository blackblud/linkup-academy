import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';

import fetchAllProfilePosts from '../../../core/store/reducers/allProfilePostsReducer/actions/allProfilePosts';
import fetchUserInfo from '../../../core/store/reducers/userReducer/actions/userInfo';
import profilePlaceholder from '../../../public/images/placeholders/profilePlaceholder.png';
import ProfilePost from '../../components/parts/ProfilePost';
import useTypedSelector from '../../hooks/useTypedSelector';
import '../../style/profile.scss';

export default function Profile(): JSX.Element {
  const history = useHistory();

  const dispatch = useDispatch();
  const user = useTypedSelector((state) => state.user);
  const posts = useTypedSelector((state) => state.profilePosts);

  useEffect(() => {
    if (!localStorage.getItem('Authorization')) {
      history.push('/posts');
    } else {
      dispatch(fetchUserInfo());
    }
  }, [dispatch, history]);

  useEffect(() => {
    if (user.username !== '') {
      dispatch(fetchAllProfilePosts(user.username));
    }
  }, [dispatch, user.username]);

  return (
    <div className="wrapper-profile">
      <div className="profile-info">
        <div className="info-main">
          <div className="main-image">
            <div>
              {user.profile_photo_url !== null ? (
                <img src={user.profile_photo_url} alt="ProfileMini" />
              ) : (
                <img src={profilePlaceholder} alt="ProfileMini" />
              )}
            </div>
          </div>
          <div className="main-data">
            <p>{`${user.first_name} ${user.last_name}`}</p>
            <p>{user.job_title}</p>
            <p>{user.description}</p>
          </div>
        </div>

        <div className="info-additional">
          <div className="addit-subscribe">
            <div>
              <div className="foll-number">{user.followers}</div>
              <div className="foll-text">Followers</div>
            </div>
            <div>
              <div className="foll-number">{user.following}</div>
              <div className="foll-text">Following</div>
            </div>
          </div>
          <div className="addit-btn">
            <Link to="profile/edit" className="addit-btn-edit" type="button">
              Edit Profile
            </Link>
            <Link to="profile/new" className="addit-btn-new" type="button">
              New Post
            </Link>
          </div>
        </div>
      </div>

      <div className="profile-info-mobile">
        <div className="user-info">
          <div className="data-info">
            <div className="data-followers">
              <p>{user.followers}</p>
              <p>Followers</p>
            </div>
            <div className="data-profilePic">
              <div>
                {user.profile_photo_url !== null ? (
                  <img src={user.profile_photo_url} alt="Profile" />
                ) : (
                  <img src={profilePlaceholder} alt="Profile" />
                )}
              </div>
            </div>
            <div className="data-following">
              <p>{user.following}</p>
              <p>Following</p>
            </div>
          </div>

          <div className="name-info">
            <p>{`${user.first_name} ${user.last_name}`}</p>
            <span>-</span>
            <p>{user.job_title}</p>
          </div>

          <div className="bio-info">
            <p>{user.description}</p>
          </div>

          <div className="button-info">
            <Link to="profile/edit" className="btn-edit-profile" type="button">
              Edit Profile
            </Link>
            <Link to="profile/new" className="btn-new-post" type="button">
              New Post
            </Link>
          </div>
        </div>
      </div>

      <div className="profile-posts">
        {posts.posts.length === undefined ? (
          <div className="container">
            <div className="loading">
              User&apos;s Posts&nbsp;
              <div className="loading__letter">L</div>
              <div className="loading__letter">o</div>
              <div className="loading__letter">a</div>
              <div className="loading__letter">d</div>
              <div className="loading__letter">i</div>
              <div className="loading__letter">n</div>
              <div className="loading__letter">g</div>
              <div className="loading__letter">.</div>
              <div className="loading__letter">.</div>
              <div className="loading__letter">.</div>
            </div>
          </div>
        ) : (
          posts.posts.map((post) => <ProfilePost data={post} key={post.id} />)
        )}
      </div>
    </div>
  );
}
