import React, { useRef } from 'react';
import { Link, useHistory } from 'react-router-dom';

import http from '../../../core/services/http';
import signPicture1 from '../../../public/images/sign/Sign1.png';
import signPicture2 from '../../../public/images/sign/Sign2.png';
import signPicture3 from '../../../public/images/sign/Sign3.png';
import signPictureMain from '../../../public/images/sign/SignMain.png';

import '../../style/sign.scss';

export default function SignIn(): JSX.Element {
  const history = useHistory();

  const formRef = useRef() as React.MutableRefObject<HTMLFormElement>;

  async function userSignIn(e: React.SyntheticEvent) {
    e.preventDefault();

    const target = e.target as typeof e.target & {
      email: { value: string };
      password: { value: string };
    };

    const dataRequest = {
      body: JSON.stringify({
        login: target.email.value,
        password: target.password.value,
      }),
    };

    const res = await http(
      'POST',
      'https://linkstagram-api.linkupst.com/login',
      dataRequest,
    );

    if (res.ok) {
      localStorage.setItem(
        'Authorization',
        String(res.headers.get('authorization')),
      );
      history.push('/posts');
    } else {
      const reg = await res.json();
      alert(
        `Error [${reg['field-error'][0]}]\n` +
          `Message - ${reg['field-error'][1]}`,
      );
    }
  }
  return (
    <div className="wrapper-sign">
      <div className="sign-content">
        <div className="sign-pictures">
          <img
            className="sign-picture-main"
            src={signPictureMain}
            alt="PicMain"
          />

          <img className="signin-picture-1" src={signPicture1} alt="Pic1" />
          <img className="signin-picture-2" src={signPicture2} alt="Pic2" />
          <img className="signin-picture-3" src={signPicture3} alt="Pic3" />
        </div>

        <form className="form-content" onSubmit={userSignIn} ref={formRef}>
          <div className="form-title">
            <h1>Log In</h1>
          </div>

          <div className="form-input">
            <p>Email</p>
            <input
              placeholder="example@mail.com"
              type="email"
              required
              name="email"
              autoComplete="on"
            />
          </div>

          <div className="form-input">
            <p>Password</p>
            <input
              placeholder="Type in..."
              type="password"
              required
              name="password"
              autoComplete="on"
            />
          </div>

          <button className="form-button" type="submit">
            <p>Log In</p>
          </button>

          <p className="p_in">
            Don&apos;t have an account?&nbsp;&nbsp;
            <Link to="/signup">Sign Up</Link>
          </p>
        </form>
      </div>
    </div>
  );
}
