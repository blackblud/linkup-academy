import React, { useRef } from 'react';
import { Link, useHistory } from 'react-router-dom';

import http from '../../../core/services/http';
import signPicture1 from '../../../public/images/sign/Sign1.png';
import signPicture2 from '../../../public/images/sign/Sign2.png';
import signPicture3 from '../../../public/images/sign/Sign3.png';
import signPictureMain from '../../../public/images/sign/SignMain.png';

import '../../style/sign.scss';

export default function SignUp(): JSX.Element {
  const history = useHistory();

  const formRef = useRef() as React.MutableRefObject<HTMLFormElement>;

  async function userSignUp(e: React.SyntheticEvent) {
    e.preventDefault();

    const target = e.target as typeof e.target & {
      email: { value: string };
      username: { value: string };
      password: { value: string };
    };

    const dataRequest = {
      body: JSON.stringify({
        username: target.username.value,
        login: target.email.value,
        password: target.password.value,
      }),
    };

    const res = await http(
      'POST',
      'https://linkstagram-api.linkupst.com/create-account',
      dataRequest,
    );

    if (res.ok) {
      history.push('/signin');
    } else {
      const reg = await res.json();
      alert(
        `Error [${reg['field-error'][0]}]\n` +
          `Message - ${reg['field-error'][1]}`,
      );
    }
  }

  return (
    <div className="wrapper-sign">
      <div className="sign-content">
        <div className="sign-pictures">
          <img
            className="sign-picture-main"
            src={signPictureMain}
            alt="PicMain"
          />

          <img className="signup-picture-1" src={signPicture1} alt="Pic1" />
          <img className="signup-picture-2" src={signPicture2} alt="Pic2" />
          <img className="signup-picture-3" src={signPicture3} alt="Pic3" />
        </div>

        <form className="form-content" onSubmit={userSignUp} ref={formRef}>
          <div className="form-title">
            <h1>Sign Up</h1>
          </div>

          <div className="form-input">
            <p>Email</p>
            <input
              placeholder="example@mail.com"
              type="email"
              required
              name="email"
            />
          </div>

          <div className="form-input">
            <p>User Name</p>
            <input placeholder="alexexample..." required name="username" />
          </div>

          <div className="form-input">
            <p>Password</p>
            <input
              placeholder="Type in..."
              type="password"
              required
              name="password"
            />
          </div>

          <button className="form-button" type="submit">
            <p>Sign up</p>
          </button>

          <p className="p_up">
            Have a account?&nbsp;&nbsp;
            <Link to="/signin">Log In</Link>
          </p>
        </form>
      </div>
    </div>
  );
}
