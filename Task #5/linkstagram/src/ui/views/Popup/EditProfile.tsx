import Uppy, { UploadedUppyFile } from '@uppy/core';
import { DashboardModal } from '@uppy/react';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import '@uppy/core/src/style.scss';
import '@uppy/dashboard/src/style.scss';
import { AwsS3, DragDrop } from 'uppy';

import fetchAPI from '../../../core/services/http';
import notAuth from '../../../core/services/notAuth';
import fetchUserInfo from '../../../core/store/reducers/userReducer/actions/userInfo';
import profilePlaceholder from '../../../public/images/placeholders/profilePlaceholder.png';
import useTypedSelector from '../../hooks/useTypedSelector';

import '../../style/popup.scss';

export default function EditProfile(): JSX.Element {
  const history = useHistory();

  const dispatch = useDispatch();
  const user = useTypedSelector((state) => state.user);

  const [upload, setUpload] = useState(false);

  const formRef = useRef() as React.MutableRefObject<HTMLFormElement>;

  useEffect(() => {
    dispatch(fetchUserInfo);
  }, [dispatch, user]);

  const uppy = new Uppy()
    .use(DragDrop, {
      width: '100%',
      height: '100%',
    })
    .use(AwsS3, {
      companionUrl: 'https://linkstagram-api.linkupst.com',
    });

  let uploadResult: UploadedUppyFile<
    Record<string, unknown>,
    Record<string, unknown>
  >[];

  uppy.on('complete', async (result) => {
    uploadResult = result.successful;

    const id = String(uploadResult[0].meta.key).substr(6);
    const filename = uploadResult[0].meta.name;
    const { size, type } = uploadResult[0].data;

    const dataRequest1 = {
      headers: {
        Authorization: String(localStorage.getItem('Authorization')),
      },
      body: JSON.stringify({
        account: {
          profile_photo: {
            id,
            storage: 'cache',
            metadata: {
              size,
              mime_type: type,
              filename,
            },
          },
        },
      }),
    };

    const res1 = await fetchAPI(
      'PATCH',
      'https://linkstagram-api.linkupst.com/account',
      dataRequest1,
    );

    if (res1.ok) {
      fetchUserInfo();
      uppy.close();
    } else {
      alert("Error While Update User's Picture. Please try again");
    }
  });

  async function userUpdateInfo(e: React.SyntheticEvent) {
    e.preventDefault();

    const target = e.target as typeof e.target & {
      firstName: { value: string };
      lastName: { value: string };
      jobTitle: { value: string };
      description: { value: string };
    };

    const dataRequest = {
      headers: {
        Authorization: String(localStorage.getItem('Authorization')),
      },
      body: JSON.stringify({
        account: {
          username: user.username,
          description: target.description.value,
          first_name: target.firstName.value,
          last_name: target.lastName.value,
          job_title: target.jobTitle.value,
        },
      }),
    };

    const res = await fetchAPI(
      'PATCH',
      'https://linkstagram-api.linkupst.com/account',
      dataRequest,
    );

    if (res.ok) {
      history.goBack();
    } else {
      alert('Error While Edit User-Data. Please try again.');
    }

    dispatch({
      type: 'UPDATE_USER',
      payload: {
        username: user.username,
        first_name: target.firstName.value,
        last_name: target.lastName.value,
        followers: user.followers,
        following: user.following,
        job_title: target.jobTitle.value,
        description: target.description.value,
        profile_photo_url: user.profile_photo_url,
      },
    });
  }

  function userLogOut() {
    localStorage.removeItem('Authorization');
    history.push('/signin');
  }

  function openModal() {
    setUpload(!upload);
  }

  return (
    <div className="modal">
      <div className="modal-edit">
        <div className="edit-header">
          <p className="editHeaderProfileInfoText">Profile Info</p>
          <button type="button" onClick={userLogOut}>
            Log out
          </button>
        </div>
        <form onSubmit={userUpdateInfo} ref={formRef}>
          <div className="edit-body">
            <div className="edit-body-top">
              <div className="body-top-image">
                <DashboardModal uppy={uppy} open={upload} />

                {user.profile_photo_url !== null ? (
                  <img src={user.profile_photo_url} alt="ProfileMini" />
                ) : (
                  <img src={profilePlaceholder} alt="ProfileMini" />
                )}

                {localStorage.getItem('Authorization') ? (
                  <button
                    className="edit-button-file"
                    type="button"
                    onClick={openModal}
                  >
                    Choose New Photo
                  </button>
                ) : (
                  <button
                    className="edit-button-file"
                    type="button"
                    onClick={notAuth}
                  >
                    Choose New Photo
                  </button>
                )}
              </div>
              <div className="body-top-input">
                <div className="edit-input">
                  <p>First Name</p>
                  <input
                    type="text"
                    defaultValue={
                      user.first_name !== null ? user.first_name : ''
                    }
                    name="firstName"
                    required
                  />
                </div>
                <div className="edit-input">
                  <p>Second Name</p>
                  <input
                    type="text"
                    defaultValue={user.last_name !== null ? user.last_name : ''}
                    name="lastName"
                    required
                  />
                </div>
              </div>
            </div>
            <div className="edit-body-bottom">
              <div className="edit-input">
                <p>Job Title</p>
                <input
                  type="text"
                  defaultValue={user.job_title !== null ? user.job_title : ''}
                  name="jobTitle"
                  required
                />
              </div>
              <div className="edit-input">
                <p id="textarea-p">Description</p>
                <textarea
                  defaultValue={
                    user.description !== null ? user.description : ''
                  }
                  name="description"
                  required
                />
              </div>
            </div>
          </div>
          <div className="edit-footer">
            <button
              onClick={history.goBack}
              className="edit-button-cancel"
              type="button"
            >
              Cancel
            </button>
            {localStorage.getItem('Authorization') ? (
              <button className="edit-button-save" type="submit">
                Save
              </button>
            ) : (
              <button
                onClick={notAuth}
                className="edit-button-save"
                type="button"
              >
                Save
              </button>
            )}
          </div>
        </form>
      </div>
      <div className="modal-edit-mobile">
        <form onSubmit={userUpdateInfo} ref={formRef}>
          <div className="edit-body">
            <div className="edit-input">
              <p>First Name</p>
              <input
                type="text"
                defaultValue={user.first_name !== null ? user.first_name : ''}
                name="firstName"
                required
              />
            </div>
            <div className="edit-input">
              <p>Second Name</p>
              <input
                type="text"
                defaultValue={user.last_name !== null ? user.last_name : ''}
                name="lastName"
                required
              />
            </div>
            <div className="edit-body-bottom">
              <div className="edit-input">
                <p>Job Title</p>
                <input
                  type="text"
                  defaultValue={user.job_title !== null ? user.job_title : ''}
                  name="jobTitle"
                  required
                />
              </div>
              <div className="edit-input">
                <p id="textarea-p">Description</p>
                <textarea
                  defaultValue={
                    user.description !== null ? user.description : ''
                  }
                  name="description"
                  required
                />
              </div>
            </div>
          </div>
          <div className="edit-footer-mobile">
            <button className="new-button-save" type="submit">
              Save
            </button>
            <button
              onClick={history.goBack}
              className="new-button-cancel"
              type="button"
            >
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
