import Uppy, { UploadedUppyFile } from '@uppy/core';
import { Dashboard } from '@uppy/react';
import React, { useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import '@uppy/core/src/style.scss';
import '@uppy/dashboard/src/style.scss';
import { AwsS3, DragDrop } from 'uppy';

import http from '../../../core/services/http';

export default function NewPost(): JSX.Element {
  const history = useHistory();
  const formRef = useRef() as React.MutableRefObject<HTMLFormElement>;

  useEffect(() => {
    if (!localStorage.getItem('Authorization')) {
      history.push('/posts');
    }
  }, [history]);

  let uploadResult: UploadedUppyFile<
    Record<string, unknown>,
    Record<string, unknown>
  >[];

  const uppy = new Uppy()
    .use(DragDrop, {
      width: '100%',
      height: '100%',
    })
    .use(AwsS3, {
      companionUrl: 'https://linkstagram-api.linkupst.com',
    });

  uppy.on('complete', (result) => {
    uploadResult = result.successful;
  });

  async function addNewPost(e: React.SyntheticEvent): Promise<void> {
    e.preventDefault();

    if (uploadResult === undefined) {
      alert('Please Upload Photos to Create a Post');
      return;
    }

    const target = e.target as typeof e.target & {
      textarea: { value: string };
    };

    const id = String(uploadResult[0].meta.key).substr(6);
    const filename = uploadResult[0].meta.name;
    const { size, type } = uploadResult[0].data;

    const dataRequest = {
      headers: {
        Authorization: String(localStorage.getItem('Authorization')),
      },
      body: JSON.stringify({
        post: {
          description: target.textarea.value,
          photos_attributes: [
            {
              image: {
                id,
                storage: 'cache',
                metadata: {
                  filename,
                  size,
                  mime_type: type,
                },
              },
            },
          ],
        },
      }),
    };

    const res = await http(
      'POST',
      'https://linkstagram-api.linkupst.com/posts',
      dataRequest,
    );

    if (res.ok) {
      history.push('/posts');
      history.go(0);
    } else {
      alert('Error. Please, try again.');
    }
  }

  return (
    <div className="modal">
      <div className="modal-new">
        <form onSubmit={addNewPost} ref={formRef}>
          <div className="new-body">
            <div className="new-body-image">
              <Dashboard uppy={uppy} height={325} />
            </div>

            <div className="new-body-desc">
              <div className="edit-input">
                <p id="textarea-p">Description</p>
                <textarea required name="textarea" />
              </div>
            </div>
          </div>

          <div className="new-footer">
            <button
              onClick={history.goBack}
              className="edit-button-cancel"
              type="button"
            >
              Cancel
            </button>
            <button className="edit-button-save" type="submit">
              Post
            </button>
          </div>
          <div className="new-footer-mobile">
            <button className="new-button-save" type="submit">
              Publish
            </button>
            <button
              onClick={history.goBack}
              className="new-button-cancel"
              type="button"
            >
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
