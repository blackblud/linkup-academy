import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';

import convertTime from '../../../core/services/convertTime';
import http from '../../../core/services/http';
import notAuth from '../../../core/services/notAuth';
import toggleLikee from '../../../core/store/reducers/allPostsReducer/actions/toggleLike';
import clearCommentsInfo from '../../../core/store/reducers/commentsReducer/actions/commentsClear';
import fetchCommentsInfo from '../../../core/store/reducers/commentsReducer/actions/commentsInfo';
import clearPostInfo from '../../../core/store/reducers/postReducer/actions/postClear';
import fetchPostInfo from '../../../core/store/reducers/postReducer/actions/postInfo';
import CatLoading from '../../../public/images/loading/cat-anim.svg';
import ProfilePlaceholder from '../../../public/images/placeholders/profilePlaceholder.png';
import TimesSVG from '../../../public/images/svg/Times.svg';
import Comment from '../../components/parts/Comment';
import useTypedSelector from '../../hooks/useTypedSelector';

interface ParamTypes {
  id: string;
}

export default function ViewPost(): JSX.Element {
  const history = useHistory();
  const { id } = useParams<ParamTypes>();

  const dispatch = useDispatch();
  const posts1 = useTypedSelector((state) => state.posts);
  const post = useTypedSelector((state) => state.post);
  const comments = useTypedSelector((state) => state.comments);

  // const posts = useTypedSelector((state) => state.posts);
  // const testPost = posts.posts.filter((testPost) => {
  //   return testPost.id === parseInt(id, 10);
  // })[0];

  // ^ ^ ^
  // | | |
  // Тут я пробував не витягувати кожен раз дані про пост, а тягнути їх з масиву постів,
  // який формую на початку загрузки сторінки. Це не получилось, так як якщо шарити комусь
  // лінк на пост, то користувач шукає пост в пустому масиві, який не заповнений.
  // Краще рішення це просто залишити "як є". Як казали, кажуть і будуть казати - "Працює - не трогай)"

  const [liked, setLiked] = useState<boolean>();
  const [changed, setChanged] = useState<boolean>(false);
  const [likesCount, setLikesCount] = useState<number>(0);

  useEffect(() => {
    dispatch(fetchPostInfo(id));
    dispatch(fetchCommentsInfo(id));

    return () => {
      dispatch(clearPostInfo());
      dispatch(clearCommentsInfo());
    };
  }, [dispatch, id]);

  useEffect(() => {
    setLiked(post.is_liked);
    setLikesCount(post.likes_count);
  }, [post.is_liked, post.likes_count]);

  const formRef = useRef() as React.MutableRefObject<HTMLFormElement>;

  async function addComment(e: React.SyntheticEvent) {
    e.preventDefault();

    const target = e.target as typeof e.target & {
      comment: { value: string };
    };

    const dataRequest = {
      headers: {
        Authorization: String(localStorage.getItem('Authorization')),
      },
      body: JSON.stringify({
        message: target.comment.value,
      }),
    };

    const res = await http(
      'POST',
      `https://linkstagram-api.linkupst.com/posts/${id}/comments`,
      dataRequest,
    );

    if (res.ok) {
      target.comment.value = '';
      dispatch(fetchCommentsInfo(id));
      dispatch(fetchPostInfo(id));
    } else {
      const reg = await res.json();
      alert(
        `Error [${reg['field-error'][0]}]\n` +
          `Message - ${reg['field-error'][1]}`,
      );
    }
  }

  function toggleLike() {
    if (!localStorage.getItem('Authorization')) {
      notAuth();
      return;
    }

    const dataRequest = {
      headers: {
        Authorization: String(localStorage.getItem('Authorization')),
      },
    };

    if (liked) {
      http(
        'DELETE',
        `https://linkstagram-api.linkupst.com/posts/${post.id}/like`,
        dataRequest,
      );

      setLikesCount(likesCount - 1);
      dispatch(toggleLikee(post.id, posts1, 'REMOVE'));
    } else {
      http(
        'POST',
        `https://linkstagram-api.linkupst.com/posts/${post.id}/like`,
        dataRequest,
      );

      setLikesCount(likesCount + 1);
      dispatch(toggleLikee(post.id, posts1, 'SET'));
    }
    setLiked(!liked);

    setChanged(!changed);

    // поміняти лайк в redux (якщо це взагалі можливо з typescript'ом).
    // Витягнути пост через filter(post => post.id == ...) не позволяє typescript
    // який каже, що має бути передано масив постів в payload.
  }

  return (
    <div className="modal">
      <div className="modal-view">
        <div className="view-image">
          {post.photos[0]?.id !== 0 || post.photos.length === 0 ? (
            <img src={post.photos[0]?.url} alt="Post" />
          ) : (
            <img src={CatLoading} alt="ViewPost Loading..." />
          )}
        </div>

        <div className="view-comment">
          <div className="comment-header">
            <div>
              {post.author.profile_photo_url !== null ? (
                <img src={post.author.profile_photo_url} alt="ProfileMini" />
              ) : (
                <img src={CatLoading} alt="Loading" className="cat-anim" />
              )}

              <p>{`${post.author.first_name} ${post.author.last_name}`}</p>
            </div>

            <button
              type="button"
              className="icon-times"
              onClick={() => {
                history.push('/posts');

                if (changed) {
                  // history.go(0);
                  // dispatch(fetchAllPosts);
                }
              }}
            >
              <img src={TimesSVG} alt="Times" />
            </button>
          </div>

          <div className="comment-body">
            <div className="comment">
              {post.author.profile_photo_url !== null ? (
                <img src={post.author.profile_photo_url} alt="ProfileMini" />
              ) : (
                <img src={ProfilePlaceholder} alt="ProfileMini" />
              )}

              <div className="comment-data">
                <p className="data-message">{post.description}</p>
                <p className="data-time">{convertTime(post.created_at)}</p>
              </div>
            </div>

            {comments.comments.map((comment) => (
              <Comment data={comment} key={comment.id} />
            ))}
          </div>

          <div className="comment-likes">
            {liked ? (
              <FontAwesomeIcon
                icon={faHeart}
                className="icon-liked"
                onClick={() => toggleLike()}
              />
            ) : (
              <FontAwesomeIcon
                icon={faHeart}
                className="icon-like"
                onClick={() => toggleLike()}
              />
            )}
            <p>{likesCount}</p>
          </div>
          <form onSubmit={addComment} ref={formRef}>
            <div className="comment-footer">
              <input
                type="text"
                placeholder="Add a comment..."
                required
                name="comment"
              />
              <button type="submit">Post</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
